# Put args into variables and handle missing args
sfeed_html_output_filepath="$1"
new_html_output_filepath="$2"

if [[ -z "$sfeed_html_output_filepath" ]]; then
  printf "No sfeed HTML file specified.\n\nUsage: $0 [sfeed HTML filepath] [output filepath]\n"
  exit 1
fi

if [[ -z "$new_html_output_filepath" ]]; then
  new_html_output_filepath="chrono.html"
fi

# Ensure errors kill process before continuing
set -euo pipefail

# Make new empty output file
echo "" > "$new_html_output_filepath"

# Create helper variables
re_feed_header='<h2 id="'
re_list_item='[0-9]{4}-[0-9]{2}-[0-9]{2}&nbsp;'
re_list_item_no_date='^<a href="'
re_end_html='</body>'
re_sidebar='id="sidebar"'

current_feed_title=""
in_html="start"
in_sidebar="false"

start_html=""
end_html="</pre>"

# Create a file of all the posts and retain useful HTML
while read line; do
  if [[ "$line" =~ $re_feed_header ]]; then
    if [[ "$in_html" == "start" ]]; then
      start_html="${start_html}\n<pre>"
      in_html="false"
    fi
    # Strip of all HTML and set current_feed_title to stripped value
    current_feed_title="${line#*>}"
    current_feed_title="${current_feed_title#*>}"
    current_feed_title="${current_feed_title%<*}"
    current_feed_title="${current_feed_title%<*}"
  elif [[ "$line" =~ $re_list_item ]]; then
    timestamp="${line%% *}"
    link="${line#* }"
    post_line="$timestamp - $current_feed_title - $link"
    echo "$post_line" >> "$new_html_output_filepath"
  elif [[ "$line" =~ $re_list_item_no_date ]]; then
    continue
  # Else if we are entering the "sidebar" (where all the feed title links are)
  elif [[ "$line" =~ $re_sidebar ]]; then
    # Make sure none of that gets into the output
    in_html="end"
    in_sidebar="true"
  elif [[ "$in_html" == "start" ]]; then
    start_html="${start_html}\n${line}"
  # If we are after all the post lines and past the sidebar
  elif [[ "$line" =~ $re_end_html ]]; then
    in_sidebar="false"
    end_html="${end_html}\n${line}"
  elif [[ "$in_html" == "end" ]] && [[ ! "$in_sidebar" == "true" ]]; then
    end_html="${end_html}\n${line}"
  fi
done < "$sfeed_html_output_filepath"

# Sort file of all the posts by name (effectively by date)
# Only include the last 500 posts
tempfile=".chrono.tmp"
sort -ro "$tempfile" "$new_html_output_filepath"
head -n 500 "$tempfile" > "$new_html_output_filepath"
rm "$tempfile"

# Prepend start_html to file and append end_html to file
ed_script="\
0a
${start_html}
.
\$a
${end_html}
.
w
"
echo "$ed_script" | ed "$new_html_output_filepath" 1>/dev/null

