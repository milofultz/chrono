====================
= CHRONO for sfeed =
====================

Creates a chronological feed from an sfeed HTML file.

USAGE
=====

1. Use `sfeed_html` to create an HTML file of your normal feed.
2. Use `chrono.sh [sfeed HTML filepath] [output filepath]` to create your chronological feed.

REQUIREMENTS
============

* sfeed
* ed

